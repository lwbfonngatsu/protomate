import 'package:flutter/material.dart';
import 'package:protomate/ui/common.dart';
import 'package:protomate/ui/zero_two_version.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
        create: (context) => StatusListener(),
      ),
      ChangeNotifierProvider(
        create: (context) => GlobalState(),
      ),
      ChangeNotifierProvider(
        create: (context) => StreamListener(),
      )
    ],
    child: MaterialApp(
        theme: ThemeData(
          primaryColor: Colors.black,
          accentColor: Colors.black12,
          buttonColor: Colors.black87,
          fontFamily: 'Georgia',
          textTheme: TextTheme(
              headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.black),
              button: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.white),
              bodyText1: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: Colors.black54),
              bodyText2: TextStyle(fontSize: 24.0, color: Colors.black87),
              headline2: TextStyle(fontSize: 30.0, color: Colors.black, fontWeight: FontWeight.bold),
              headline3: TextStyle(fontSize: 30.0, color: Colors.white, fontWeight: FontWeight.bold),
              headline4: TextStyle(fontSize: 30.0, color: Colors.white),
              headline5: TextStyle(fontSize: 24.0, color: Colors.black, fontWeight: FontWeight.bold),
              headline6: TextStyle(fontSize: 20.0, color: Colors.white)),
        ),
        debugShowCheckedModeBanner: false,
        home: MainNavigateBar()),
  ));
}
