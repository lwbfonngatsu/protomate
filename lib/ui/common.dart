import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:async/async.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:janus_client/Plugin.dart';
import 'package:janus_client/janus_client.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:janus_client/utils.dart';
import 'package:provider/provider.dart';

class HelmetStatus extends StatefulWidget {
  @override
  _HelmetStatusState createState() => _HelmetStatusState();
}

class _HelmetStatusState extends State<HelmetStatus> {
  Timer _refreshTimer;

  @override
  void initState() {
    _refreshTimer = Timer.periodic(Duration(seconds: 1), (Timer t) => setState(() {}));
    super.initState();
  }

  @override
  void dispose() {
    _refreshTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.03,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
            child: AutoSizeText(
              "Helmet Status",
              style: Theme.of(context).textTheme.bodyText2,
              maxLines: 1,
              maxFontSize: 14,
            ),
          ),
          Row(
            children: <Widget>[
              Opacity(
                opacity: context.watch<StatusListener>().wifiStatus ? 1.0 : 0.0,
                child: Icon(
                  Icons.wifi,
                  color: Colors.black,
                  size: 18,
                ),
              ),
              Opacity(
                opacity: context.watch<StatusListener>().blueStatus ? 1.0 : 0.0,
                child: Icon(
                  Icons.bluetooth,
                  color: Colors.black,
                  size: 18,
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.sd_card,
                color: Colors.black,
                size: 18,
              ),
              AutoSizeText(
                "${context.watch<StatusListener>().currStorage} / ${context.watch<StatusListener>().maxStorage} GB",
                style: Theme.of(context).textTheme.bodyText2,
                maxFontSize: 14,
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
            child: Row(
              children: <Widget>[
                Transform.rotate(angle: 22 / 7 * 0.5, child: Icon(Icons.battery_full)),
                AutoSizeText(
                  "${context.watch<StatusListener>().currBatt} %",
                  style: Theme.of(context).textTheme.bodyText2,
                  maxFontSize: 14,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ComingSoonPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            right: -40.0,
            top: -40.0,
            child: InkResponse(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: CircleAvatar(
                child: Icon(Icons.close),
                backgroundColor: Colors.red,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.5,
            height: MediaQuery.of(context).size.height * 0.2,
            child: Center(
              child: AutoSizeText(
                "Coming Soon...",
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class HorizontalLine extends StatelessWidget {
  final double lheight;
  final double lwidth;
  final Color color;

  const HorizontalLine({Key key, this.lheight = 0.5, this.color = Colors.black, this.lwidth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: lwidth,
      height: lheight,
      color: color,
    );
  }
}

class LoadingAnimation extends StatefulWidget {
  final double radius;
  final double dotRadius;

  LoadingAnimation({this.radius = 30.0, this.dotRadius = 3.0});

  @override
  _LoadingAnimationState createState() => _LoadingAnimationState();
}

class _LoadingAnimationState extends State<LoadingAnimation> with SingleTickerProviderStateMixin {
  Animation<double> animationRotation;
  Animation<double> animationRadiusIn;
  Animation<double> animationRadiusOut;
  AnimationController controller;

  double radius;
  double dotRadius;

  @override
  void initState() {
    super.initState();

    radius = widget.radius;
    dotRadius = widget.dotRadius;
    controller = AnimationController(lowerBound: 0.0, upperBound: 1.0, duration: const Duration(milliseconds: 3000), vsync: this);
    animationRotation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.0, 1.0, curve: Curves.linear),
      ),
    );

    animationRadiusIn = Tween(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.75, 1.0, curve: Curves.elasticIn),
      ),
    );

    animationRadiusOut = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: controller,
        curve: Interval(0.0, 0.25, curve: Curves.elasticOut),
      ),
    );

    controller.addListener(() {
      setState(() {
        if (controller.value >= 0.75 && controller.value <= 1.0)
          radius = widget.radius * animationRadiusIn.value;
        else if (controller.value >= 0.0 && controller.value <= 0.25) radius = widget.radius * animationRadiusOut.value;
      });
    });

    controller.addStatusListener((status) {
      if (status == AnimationStatus.completed) {}
    });

    controller.repeat();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100.0,
      height: 100.0,
      child: new Center(
        child: new RotationTransition(
          turns: animationRotation,
          child: new Container(
            //color: Colors.limeAccent,
            child: new Center(
              child: Stack(
                children: <Widget>[
                  new Transform.translate(
                    offset: Offset(0.0, 0.0),
                    child: Dot(
                      radius: radius,
                      color: Colors.black,
                    ),
                  ),
                  new Transform.translate(
                    child: Dot(
                      radius: dotRadius,
                      color: Colors.amber,
                    ),
                    offset: Offset(
                      radius * cos(0.0),
                      radius * sin(0.0),
                    ),
                  ),
                  new Transform.translate(
                    child: Dot(
                      radius: dotRadius,
                      color: Colors.deepOrangeAccent,
                    ),
                    offset: Offset(
                      radius * cos(0.0 + 1 * pi / 4),
                      radius * sin(0.0 + 1 * pi / 4),
                    ),
                  ),
                  new Transform.translate(
                    child: Dot(
                      radius: dotRadius,
                      color: Colors.pinkAccent,
                    ),
                    offset: Offset(
                      radius * cos(0.0 + 2 * pi / 4),
                      radius * sin(0.0 + 2 * pi / 4),
                    ),
                  ),
                  new Transform.translate(
                    child: Dot(
                      radius: dotRadius,
                      color: Colors.purple,
                    ),
                    offset: Offset(
                      radius * cos(0.0 + 3 * pi / 4),
                      radius * sin(0.0 + 3 * pi / 4),
                    ),
                  ),
                  new Transform.translate(
                    child: Dot(
                      radius: dotRadius,
                      color: Colors.yellow,
                    ),
                    offset: Offset(
                      radius * cos(0.0 + 4 * pi / 4),
                      radius * sin(0.0 + 4 * pi / 4),
                    ),
                  ),
                  new Transform.translate(
                    child: Dot(
                      radius: dotRadius,
                      color: Colors.lightGreen,
                    ),
                    offset: Offset(
                      radius * cos(0.0 + 5 * pi / 4),
                      radius * sin(0.0 + 5 * pi / 4),
                    ),
                  ),
                  new Transform.translate(
                    child: Dot(
                      radius: dotRadius,
                      color: Colors.orangeAccent,
                    ),
                    offset: Offset(
                      radius * cos(0.0 + 6 * pi / 4),
                      radius * sin(0.0 + 6 * pi / 4),
                    ),
                  ),
                  new Transform.translate(
                    child: Dot(
                      radius: dotRadius,
                      color: Colors.blueAccent,
                    ),
                    offset: Offset(
                      radius * cos(0.0 + 7 * pi / 4),
                      radius * sin(0.0 + 7 * pi / 4),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class Dot extends StatelessWidget {
  final double radius;
  final Color color;

  Dot({this.radius, this.color});

  @override
  Widget build(BuildContext context) {
    return new Center(
      child: Container(
        width: radius,
        height: radius,
        decoration: BoxDecoration(color: color, shape: BoxShape.circle),
      ),
    );
  }
}

class StatusListener extends ChangeNotifier {
  bool connected = false;

  BluetoothConnection connection;
  int contentLength = 0;
  List<List<int>> chunks = <List<int>>[];
  Uint8List _bytes;

  Timer _sendTimer;
  RestartableTimer _timer;

  String currBatt = "0";
  String currStorage = "0";
  String maxStorage = "0";
  bool wifiStatus = false;
  bool blueStatus = false;

  void startBlueService() {
    connection.input.listen(_onDataReceived).onDone(() {
      sendMessage("r");
    });
    _timer = new RestartableTimer(Duration(seconds: 1), _getData);
    _sendTimer = Timer.periodic(Duration(seconds: 5), (Timer t) => sendMessage("r"));
  }

  void stopBlueService() {
    connection.dispose();
    _timer.cancel();
    _sendTimer.cancel();
  }

  @override
  void dispose() {
    connection.close();
    _timer.cancel();
    _sendTimer.cancel();
    super.dispose();
  }

  void sendMessage(String text) async {
    text = text.trim();
    if (text.length > 0) {
      try {
        connection.output.add(utf8.encode(text));
        await connection.output.allSent;
      } catch (error) {
        debugPrint("Got Error: $error");
        dispose();
      }
    }
  }

  void _onDataReceived(Uint8List data) {
    if (data != null && data.length > 0) {
      chunks.add(data);
      contentLength += data.length;
      _timer.reset();
    }
    print("Data length: ${data.length}, chunks: ${chunks.length}");
  }

  _getData() {
    if (chunks.length == 0 || contentLength == 0) return;
    _bytes = Uint8List(contentLength);
    int offset = 0;
    for (final List<int> chunk in chunks) {
      _bytes.setRange(offset, offset + chunk.length, chunk);
      offset += chunk.length;
    }

    contentLength = 0;
    chunks.clear();
    String receivedData = utf8.decode(_bytes);
    print("Got: $receivedData");

    if (receivedData[0] == "r") _extractBatteryData(receivedData);
  }

  _extractBatteryData(String data) {
    List<String> splited = data.substring(1).trim().split(",");
    currBatt = splited[0];
    currStorage = splited[1];
    maxStorage = splited[2];
    wifiStatus = (splited[3] == '1') ? true : false;
    blueStatus = (splited[4] == '1') ? true : false;
  }
}

class StreamListener extends ChangeNotifier {
  bool isConnectingToStream = false;
  bool streamConnected = false;
  RTCVideoRenderer remoteRenderer = new RTCVideoRenderer();
  JanusClient janusClient = JanusClient(iceServers: [
    RTCIceServer(url: "stun:40.85.216.95:3478", username: "onemandev", credential: "SecureIt"),
    RTCIceServer(url: "turn:40.85.216.95:3478", username: "onemandev", credential: "SecureIt")
  ], server: [
    'https://janus.conf.meetecho.com',
  ], withCredentials: true, apiSecret: "SecureIt");
  Plugin streaming;

  connectStream() {
    try {
      isConnectingToStream = true;
      janusClient.connect(onSuccess: (sessionId) {
        janusClient.attach(Plugin(
          plugin: "janus.plugin.streaming",
          onSuccess: (plugin) {
            streaming = plugin;
            if (streaming != null) {
              streaming.send(
                  message: {"request": "watch", "id": 1},
                  onSuccess: () {
                    debugPrint("Successfully mount to a stream...");
                  });
              streaming.send(
                  message: {"request": "start"},
                  onSuccess: () {
                    isConnectingToStream = false;
                    streamConnected = true;
//                    notifyListeners();
                  });
            }
          },
          onMessage: (msg, jsep) async {
            debugPrint("Receive message: $msg");
            if (jsep != null) streaming.handleRemoteJsep(jsep);
            try {
              var offer = await streaming.createAnswer();
              var body = {"request": "start"};
              streaming.send(
                message: body,
                jsep: offer,
              );
            } catch (e) {
              debugPrint("Got error: $e");
            }
          },
          onRemoteStream: (remoteStream) {
            remoteRenderer.srcObject = remoteStream;
          },
        ));
      });
    } catch (e) {
      debugPrint("Got error: $e");
    }
  }

  startStream() {
    streaming.send(
      message: {"request": "start"},
    );
  }

  Future<void> cleanupStream() async {
    streaming.send(
      message: {"request": "pause"},
    );
  }
}

class GlobalState extends ChangeNotifier {
  int currentPageIndex = 0;

  changeCurrentPageIndex(int index) {
    currentPageIndex = index;
    notifyListeners();
  }
}
