import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:protomate/backend/image_loader.dart';
import 'package:protomate/ui/common.dart';

class ImagesListPage extends StatefulWidget {
  @override
  _ImagesListPageState createState() => _ImagesListPageState();
}

class _ImagesListPageState extends State<ImagesListPage> {
  bool show = true;
  int showingIndex = 0;
  final List<Images> imagesList = Images.getImages();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () => changeDisplayingState(),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
          ),
          GestureDetector(
            onTap: () => changeDisplayingState(),
            child: Center(
              child: Container(
                child: PhotoView(
                  imageProvider: AssetImage(imagesList[showingIndex].image),
                ),
              ),
            ),
          ),
          AnimatedOpacity(
            duration: Duration(milliseconds: 300),
            opacity: show ? 1.0 : 0.0,
            child: Column(
              children: <Widget>[
                HelmetStatus(),
                FileDate(
                  date: imagesList[showingIndex].date,
                  time: imagesList[showingIndex].time,
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.6),
                Flexible(
                  child: Container(
                    color: Colors.black54,
                    child: ListView.builder(
                      itemCount: imagesList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () => setState(() {
                            showingIndex = index;
                          }),
                          child: Padding(
                            padding: const EdgeInsets.only(right: 2.0),
                            child: Container(width: MediaQuery.of(context).size.width * 0.2, child: Image.asset(imagesList[index].image)),
                          ),
                        );
                      },
                      scrollDirection: Axis.horizontal,
                    ),
                  ),
                ),
                Container(
                  color: Colors.black54,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.01),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Icon(
                                Icons.file_download,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Icon(
                                Icons.delete_outline,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: InkWell(
        onTap: show ? () => Navigator.pop(context) : null,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          color: Theme.of(context).primaryColor,
          child: Center(
            child: AnimatedOpacity(
              opacity: show ? 1.0 : 0.0,
              duration: Duration(milliseconds: 300),
              child: AutoSizeText(
                "BACK",
                style: Theme.of(context).textTheme.button,
                maxFontSize: 20,
              ),
            ),
          ),
        ),
      ),
    );
  }

  changeDisplayingState() {
    setState(() {
      show = !show;
    });
  }
}

class FileDate extends StatelessWidget {
  final String date;
  final String time;

  const FileDate({Key key, this.date, this.time}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.1,
      color: Colors.black54,
      child: Padding(
        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
        child: Column(
          children: <Widget>[
            Center(
              child: AutoSizeText(
                "$date",
                style: Theme.of(context).textTheme.headline4,
                maxFontSize: 20,
                maxLines: 1,
              ),
            ),
            Center(
              child: AutoSizeText(
                "$time",
                style: Theme.of(context).textTheme.headline6,
                maxFontSize: 12,
                maxLines: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
