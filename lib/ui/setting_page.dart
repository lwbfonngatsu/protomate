import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:protomate/ui/common.dart';
import 'package:protomate/ui/hud_setting_page.dart';
import 'package:protomate/ui/camera_setting_page.dart';

class SettingPage extends StatelessWidget {
  const SettingPage();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Theme.of(context).accentColor,
        ),
        Column(
          children: <Widget>[
            HelmetStatus(),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
              child: Center(
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.95,
                    height: MediaQuery.of(context).size.height * 0.175,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.white),
                    child: Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.95,
                            height: MediaQuery.of(context).size.height * 0.05,
                            decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5)), color: Theme.of(context).buttonColor),
                            child: Center(
                              child: AutoSizeText(
                                "Setting",
                                style: Theme.of(context).textTheme.headline4,
                                maxFontSize: 24,
                              ),
                            ),
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                                  child: Container(
                                    height: MediaQuery.of(context).size.height * 0.15,
                                    child: Image.asset(
                                      "images/Motorbike_Helmet.png",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.04),
                                  child: Container(
                                    height: MediaQuery.of(context).size.height * 0.05,
                                    child: Image.asset(
                                      "images/Protomate_logo.png",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        height: MediaQuery.of(context).size.height * 0.03,
                                        child: Image.asset(
                                          "images/Protomate_name.png",
                                        ),
                                      ),
                                      AutoSizeText(
                                        "Some motto",
                                        style: Theme.of(context).textTheme.bodyText2,
                                        maxFontSize: 12,
                                      )
                                    ],
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ],
                    )),
              ),
            ),
            Spacer(),
            Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
              child: InkWell(
                onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (context) => CameraSettingPage())),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.camera_alt,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: AutoSizeText(
                          "Cameras Setting",
                          style: Theme.of(context).textTheme.headline4,
                          maxFontSize: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
              child: InkWell(
                onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (context) => HUDSettingPage())),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.tv,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: AutoSizeText(
                          "HUD Display Setting",
                          style: Theme.of(context).textTheme.headline4,
                          maxFontSize: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
              child: InkWell(
                onTap: () => showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ComingSoonPopup();
                    }),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.settings,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: AutoSizeText(
                          "Application Setting",
                          style: Theme.of(context).textTheme.headline4,
                          maxFontSize: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
              child: InkWell(
                onTap: () => showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ComingSoonPopup();
                    }),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.help_outline,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: AutoSizeText(
                          "Get Help",
                          style: Theme.of(context).textTheme.headline4,
                          maxFontSize: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
              child: InkWell(
                onTap: () => showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ComingSoonPopup();
                    }),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.monetization_on,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: AutoSizeText(
                          "ProtoMate Reward",
                          style: Theme.of(context).textTheme.headline4,
                          maxFontSize: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Spacer()
          ],
        )
      ],
    );
  }
}
