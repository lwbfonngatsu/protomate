import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:protomate/backend/BluetoothDeviceListEntry.dart';
import 'package:provider/provider.dart';
import 'package:protomate/ui/common.dart';

class BluetoothPage extends StatefulWidget {
  @override
  _BluetoothPageState createState() => _BluetoothPageState();
}

class _BluetoothPageState extends State<BluetoothPage> with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  BluetoothState _bluetoothState = BluetoothState.UNKNOWN;
  StreamSubscription<BluetoothDiscoveryResult> _streamSubscription;
  List<BluetoothDiscoveryResult> devices = List<BluetoothDiscoveryResult>();
  bool finishDiscover;
  bool discovering;

  BluetoothConnection connection;
  bool isConnecting;

  bool get isConnected => connection != null && connection.isConnected;
  bool isDisconnecting = false;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    devices.clear();
    discovering = false;
    finishDiscover = false;
    isConnecting = false;
    _stateChangeListener();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    if (isConnected) {
      isDisconnecting = true;
      connection.dispose();
      connection = null;
    }
    super.dispose();
  }

  _stateChangeListener() {
    FlutterBluetoothSerial.instance.onStateChanged().listen((BluetoothState state) {
      _bluetoothState = state;
      if (!_bluetoothState.isEnabled) {
        devices.clear();
      }
      print("State isEnabled: ${state.isEnabled}");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var helmetStatus = Provider.of<StatusListener>(context);
    SystemChrome.setEnabledSystemUIOverlays([]);
    return (!discovering)
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.50,
                    color: Theme.of(context).accentColor,
                  ),
                  Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
                        child: Center(
                          child: Container(
                            child: AutoSizeText(
                              "ProtoMate",
                              style: Theme.of(context).textTheme.headline1,
                              maxLines: 1,
                              maxFontSize: 56,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.040),
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          height: MediaQuery.of(context).size.height * 0.30,
                          child: Image.asset(
                            'images/Home_Protomate.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              if (!discovering & !finishDiscover)
                Hint()
              else if (!discovering & finishDiscover & (devices.length == 0))
                AutoSizeText(
                  "Not found nearby device.",
                  style: Theme.of(context).textTheme.bodyText2,
                  textAlign: TextAlign.center,
                  maxFontSize: 18,
                  maxLines: 1,
                )
              else
                Flexible(
                  child: ListView(
                    children: devices
                        .map((_device) => BluetoothDeviceListEntry(
                              device: _device,
                              enabled: true,
                              onTap: !discovering
                                  ? () async {
                                      isConnecting = true;
                                      if (_device.device.isBonded) {
                                        try {
                                          print('Unbonding from ${_device.device.address}...');
                                          await FlutterBluetoothSerial.instance.removeDeviceBondWithAddress(_device.device.address);
                                          print('Unbonding from ${_device.device.address} has success');
                                        } on Exception catch (_) {
                                          print("Unbonded");
                                        }
                                      }
                                      try {
                                        bool bonded = false;
                                        print('Bonding with ${_device.device.address}...');
                                        bonded = await FlutterBluetoothSerial.instance.bondDeviceAtAddress(_device.device.address);
                                        print('Bonding with ${_device.device.address} has ${bonded ? 'success' : 'failed'}.');
                                      } on Exception catch (_) {
                                        print("Bonded");
                                      }

                                      BluetoothConnection.toAddress(_device.device.address).then((_connection) {
                                        print("Connected");
                                        setState(() {
                                          connection = _connection;
                                          isConnecting = false;
                                          isDisconnecting = false;
                                        });
                                      }).catchError((error) {
                                        print("Connection Error: $error");
                                      }).then((_) {
                                        if (isConnected) {
                                          helmetStatus.connection = connection;
                                          helmetStatus.startBlueService();
                                          helmetStatus.connected = true;
                                          Provider.of<GlobalState>(context, listen: false).changeCurrentPageIndex(1);
                                        }
                                      });
                                    }
                                  : null,
                            ))
                        .toList(),
                  ),
                ),
              Padding(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.01),
                child: InkWell(
                  onTap: () => searchDevice(),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.06,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5.0), color: Theme.of(context).buttonColor),
                    child: Center(
                      child: Container(
                        child: AutoSizeText(
                          "DISCOVER",
                          style: Theme.of(context).textTheme.button,
                          maxLines: 1,
                          maxFontSize: 24,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        : Stack(
            children: <Widget>[
              Opacity(
                opacity: 0.3,
                child: Scaffold(
                  body: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height * 0.50,
                            color: Theme.of(context).accentColor,
                          ),
                          Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
                                child: Center(
                                  child: Container(
                                    child: AutoSizeText(
                                      "ProtoMate",
                                      style: Theme.of(context).textTheme.headline1,
                                      maxLines: 1,
                                      maxFontSize: 56,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.040),
                                child: Container(
                                  width: MediaQuery.of(context).size.width * 0.9,
                                  height: MediaQuery.of(context).size.height * 0.30,
                                  child: Image.asset(
                                    'images/Home_Protomate.png',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      (!discovering & !finishDiscover)
                          ? Hint()
                          : Flexible(
                              child: ListView(
                                children: devices
                                    .map((_device) => BluetoothDeviceListEntry(
                                          device: _device,
                                          enabled: true,
                                        ))
                                    .toList(),
                              ),
                            ),
                      Padding(
                        padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
                        child: InkWell(
                          onTap: () => null,
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.8,
                            height: MediaQuery.of(context).size.height * 0.06,
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(5.0), color: Theme.of(context).buttonColor),
                            child: Center(
                              child: Container(
                                child: AutoSizeText(
                                  "DISCOVER",
                                  style: Theme.of(context).textTheme.button,
                                  maxLines: 1,
                                  maxFontSize: 24,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Center(child: LoadingAnimation(radius: 50, dotRadius: 10)),
            ],
          );
  }

  searchDevice() {
    if (discovering) {
      return null;
    } else {
      future() async {
        await FlutterBluetoothSerial.instance.requestEnable();
      }

      future().then((_) {
        startDiscovery() async {
          devices.clear();
          _streamSubscription = FlutterBluetoothSerial.instance.startDiscovery().listen((r) {
            setState(() {
              devices.add(r);
              discovering = true;
            });
          });
          _streamSubscription.onDone(() {
            setState(() {
              discovering = false;
              finishDiscover = true;
            });
          });
        }

        startDiscovery();
      });
    }
  }
}

class Hint extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
      child: Center(
        child: AutoSizeText(
          "Click \"DISCOVER\" button to\nsearch for your helmet.",
          style: Theme.of(context).textTheme.bodyText2,
          textAlign: TextAlign.center,
          maxLines: 2,
          maxFontSize: 18,
        ),
      ),
    );
  }
}
