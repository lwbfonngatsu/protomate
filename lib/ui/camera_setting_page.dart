import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:janus_client/Plugin.dart';
import 'package:janus_client/janus_client.dart';
import 'package:janus_client/utils.dart';
import 'package:protomate/backend/camera_preset_repo.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'common.dart';

//TODO Bluetooth service
//When change Preset -> device : device change cameras setting
//device send mobile selecting Camera view -> mobile app
//When change Camera -> device : device send camera view respond to mobile app
//TODO Custom Stage
//When change some option will change other options in next camera setting !@#$%^&

class CameraSettingPage extends StatefulWidget {
  @override
  _CameraSettingPageState createState() => _CameraSettingPageState();
}

class _CameraSettingPageState extends State<CameraSettingPage> {
  List<String> presets = ['Preset 1', 'Preset 2', 'Preset 3', 'Preset 4', 'Preset 5', 'Custom'];
  String _currentPreset = "Preset 1";

  List<String> cameras = ['Front-wide', 'Front-tele', 'Left', 'Rear', 'Right'];
  String _currentCamera = "Front-wide";

  List<String> resolutions = ['4K', 'Full HD', '720P'];
  String _currentRes = "4K";

  List<String> FPSs = ['60 FPS', '30 FPS'];
  String _currentFPS = "60 FPS";

  Map<String, Map<String, List<String>>> availableChoices = {
    'Front-wide': {
      'res': ['4K', 'Full HD', '720P'],
      'fps': ['60 FPS', '30 FPS']
    },
    'Front-tele': {
      'res': ['4K', 'Full HD', '720P'],
      'fps': ['60 FPS', '30 FPS']
    },
    'Left': {
      'res': ['Full HD', '720P'],
      'fps': ['60 FPS', '30 FPS']
    },
    'Rear': {
      'res': ['4K', 'Full HD', '720P'],
      'fps': ['60 FPS', '30 FPS']
    },
    'Right': {
      'res': ['Full HD', '720P'],
      'fps': ['60 FPS', '30 FPS']
    },
  };

  int used = 0;
  bool isCustoming = false;
  bool finishedCustom = false;
  Map presetsCFG = PresetRepo.getPreset();
  int cIndex = 0;
  int memory = 8192;
  Map<String, int> usage = {'4K': 1024, 'Full HD': 512, '720P': 384, '60 FPS': 1024, '30 FPS': 512};
  Map<String, Map<String, String>> applied = {};

  bool isConnectingToStream = false;
  JanusClient janusClient = JanusClient(iceServers: [
    RTCIceServer(url: "stun:40.85.216.95:3478", username: "onemandev", credential: "SecureIt"),
    RTCIceServer(url: "turn:40.85.216.95:3478", username: "onemandev", credential: "SecureIt")
  ], server: [
//    'http://10.0.11.201:8088',
    'https://janus.conf.meetecho.com',
  ], withCredentials: true, apiSecret: "SecureIt");
  RTCVideoRenderer _remoteRenderer = new RTCVideoRenderer();
  Plugin streaming;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    await _remoteRenderer.initialize();
  }

  @override
  void initState() {
    super.initState();
    try {
      isConnectingToStream = true;
      janusClient.connect(onSuccess: (sessionId) {
        janusClient.attach(Plugin(
          plugin: "janus.plugin.streaming",
          onSuccess: (plugin) {
            setState(() {
              streaming = plugin;
              if (streaming != null) {
                streaming.send(
                    message: {"request": "watch", "id": 1},
                    onSuccess: () {
                      debugPrint("Successfully mount to a stream...");
                    });
                streaming.send(
                    message: {"request": "start"},
                    onSuccess: () {
                      setState(() {
                        isConnectingToStream = false;
                      });
                    });
              }
            });
          },
          onMessage: (msg, jsep) async {
            debugPrint("Receive message: $msg");
            if (jsep != null) streaming.handleRemoteJsep(jsep);
            try {
              var offer = await streaming.createAnswer();
              var body = {"request": "start"};
              streaming.send(
                message: body,
                jsep: offer,
              );
            } catch (e) {
              debugPrint("Got error: $e");
            }
          },
          onRemoteStream: (remoteStream) {
            _remoteRenderer.srcObject = remoteStream;
          },
        ));
      });
    } catch (e) {
      debugPrint("Got error: $e");
      Navigator.pop(context);
    }
  }

  Future<void> cleanUpAndBack() async {
    streaming.send(
      message: {"request": "stop"},
    );
    janusClient.destroy();
    await _remoteRenderer.dispose();
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return (!isConnectingToStream)
        ? Scaffold(
            body: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Theme.of(context).accentColor,
                ),
                Column(
                  children: <Widget>[
//              HelmetStatus(),
                    Container(
                      height: (MediaQuery.of(context).size.width / 16) * 9,
                      color: Colors.black,
                      child: RTCVideoView(
                        _remoteRenderer,
                        mirror: false,
                        objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitContain,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            height: MediaQuery.of(context).size.height * 0.45,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.6,
                            child: Column(
                              children: <Widget>[
                                HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.6),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.6),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                // Preset
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    AutoSizeText("Preset", style: Theme.of(context).textTheme.headline5, maxLines: 1),
                                    Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                      child: DropdownButtonHideUnderline(
                                        child: Container(
                                          color: Colors.white,
                                          width: MediaQuery.of(context).size.width * 0.32,
                                          child: DropdownButton<String>(
                                            value: _currentPreset,
                                            icon: Icon(Icons.arrow_drop_down),
                                            items: presets.map<DropdownMenuItem<String>>((String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                  child: AutoSizeText(
                                                    value,
                                                    style: Theme.of(context).textTheme.bodyText2,
                                                    maxFontSize: 15,
                                                  ),
                                                ),
                                              );
                                            }).toList(),
                                            onChanged: (newPreset) {
                                              setState(() {
                                                _currentPreset = newPreset;
                                                _currentCamera = cameras[0];
                                                _currentRes = resolutions[0];
                                                _currentFPS = FPSs[0];
                                                if (_currentPreset == "Custom") {
                                                  isCustoming = true;
                                                  updateSelected();
                                                } else
                                                  isCustoming = false;
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.6),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.6),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                // Camera
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    AutoSizeText(
                                      "Camera",
                                      style: Theme.of(context).textTheme.headline5,
                                      maxLines: 1,
                                      maxFontSize: 15,
                                    ),
                                    Container(
                                      color: Colors.white,
                                      width: MediaQuery.of(context).size.width * 0.32,
                                      height: MediaQuery.of(context).size.height * 0.07,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          isCustoming
                                              ? InkWell(
                                                  child: Icon(Icons.arrow_back_ios, size: MediaQuery.of(context).size.width * 0.04, color: Colors.grey.shade100),
                                                  onTap: () => null,
                                                )
                                              : InkWell(
                                                  child: Icon(Icons.arrow_back_ios, size: MediaQuery.of(context).size.width * 0.04),
                                                  onTap: () => decreaseCameraIndex(),
                                                ),
                                          AutoSizeText(
                                            "${cameras[cIndex]}",
                                            style: Theme.of(context).textTheme.bodyText2,
                                            maxFontSize: 15,
                                          ),
                                          isCustoming
                                              ? InkWell(
                                                  child: Icon(Icons.arrow_forward_ios, size: MediaQuery.of(context).size.width * 0.04, color: Colors.grey.shade100),
                                                  onTap: () => null,
                                                )
                                              : InkWell(
                                                  child: Icon(Icons.arrow_forward_ios, size: MediaQuery.of(context).size.width * 0.04),
                                                  onTap: () => increaseCameraIndex(),
                                                ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                // Resolution
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    AutoSizeText(
                                      "Resolution",
                                      style: Theme.of(context).textTheme.headline5,
                                      maxLines: 1,
                                      maxFontSize: 15,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                      child: DropdownButtonHideUnderline(
                                        child: Container(
                                          color: Colors.white,
                                          width: MediaQuery.of(context).size.width * 0.32,
                                          height: MediaQuery.of(context).size.height * 0.07,
                                          child: DropdownButton<String>(
                                            value: _currentRes,
                                            icon: Icon(Icons.arrow_drop_down),
                                            items: availableChoices[_currentCamera]['res'].map<DropdownMenuItem<String>>((String value) {
                                              return DropdownMenuItem<String>(
                                                value: value,
                                                child: Padding(
                                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                  child: AutoSizeText(
                                                    value,
                                                    style: Theme.of(context).textTheme.bodyText2,
                                                    maxFontSize: 15,
                                                  ),
                                                ),
                                              );
                                            }).toList(),
                                            onChanged: isCustoming
                                                ? (newRes) {
                                                    setState(() {
                                                      _currentRes = newRes;
                                                      updateSelected();
                                                    });
                                                  }
                                                : null,
                                            disabledHint: isCustoming
                                                ? null
                                                : Padding(
                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                    child: AutoSizeText(
                                                      '${presetsCFG[_currentPreset][_currentCamera].resolution}',
                                                      style: Theme.of(context).textTheme.bodyText2,
                                                      maxFontSize: 15,
                                                    ),
                                                  ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                // Frame rate
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    AutoSizeText(
                                      "Frame rate",
                                      style: Theme.of(context).textTheme.headline5,
                                      maxLines: 1,
                                      maxFontSize: 15,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                      child: DropdownButtonHideUnderline(
                                        child: Container(
                                          color: Colors.white,
                                          width: MediaQuery.of(context).size.width * 0.32,
                                          height: MediaQuery.of(context).size.height * 0.07,
                                          child: DropdownButton<String>(
                                              value: _currentFPS,
                                              icon: Icon(Icons.arrow_drop_down),
                                              items: availableChoices[_currentCamera]['fps'].map<DropdownMenuItem<String>>((String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                    child: AutoSizeText(
                                                      value,
                                                      style: Theme.of(context).textTheme.bodyText2,
                                                      maxFontSize: 15,
                                                    ),
                                                  ),
                                                );
                                              }).toList(),
                                              onChanged: isCustoming
                                                  ? (newFPS) {
                                                      setState(() {
                                                        _currentFPS = newFPS;
                                                        updateSelected();
                                                      });
                                                    }
                                                  : null,
                                              disabledHint: isCustoming
                                                  ? null
                                                  : Padding(
                                                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                      child: AutoSizeText(
                                                        '${presetsCFG[_currentPreset][_currentCamera].FPS}',
                                                        style: Theme.of(context).textTheme.bodyText2,
                                                        maxFontSize: 15,
                                                      ),
                                                    )),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: MediaQuery.of(context).size.height * 0.04),
                                // Button
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Opacity(
                                      opacity: isCustoming ? 1.0 : 0.0,
                                      child: finishedCustom ? addPresetButton() : customPrevButton(),
                                    ),
                                    isCustoming ? customNextButton() : customApplyButton()
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
            bottomNavigationBar: InkWell(
              onTap: () => cleanUpAndBack(),
              child: Container(
                height: MediaQuery.of(context).size.height * 0.1,
                color: Theme.of(context).primaryColor,
                child: Center(
                  child: AutoSizeText(
                    "BACK",
                    style: Theme.of(context).textTheme.button,
                    maxFontSize: 20,
                  ),
                ),
              ),
            ),
          )
        : Stack(
            children: <Widget>[
              Opacity(
                opacity: 0.3,
                child: Scaffold(
                  body: Stack(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        color: Theme.of(context).accentColor,
                      ),
                      Column(
                        children: <Widget>[
//              HelmetStatus(),
                          Container(
                            height: (MediaQuery.of(context).size.width / 16) * 9,
                            color: Colors.black,
                            child: RTCVideoView(
                              _remoteRenderer,
                              mirror: false,
                              objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitContain,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width * 0.3,
                                  height: MediaQuery.of(context).size.height * 0.45,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width * 0.6,
                                  child: Column(
                                    children: <Widget>[
                                      HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.6),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                      HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.6),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                      // Preset
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          AutoSizeText("Preset", style: Theme.of(context).textTheme.headline5, maxLines: 1),
                                          Padding(
                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                            child: DropdownButtonHideUnderline(
                                              child: Container(
                                                color: Colors.white,
                                                width: MediaQuery.of(context).size.width * 0.32,
                                                child: DropdownButton<String>(
                                                  value: _currentPreset,
                                                  icon: Icon(Icons.arrow_drop_down),
                                                  items: presets.map<DropdownMenuItem<String>>((String value) {
                                                    return DropdownMenuItem<String>(
                                                      value: value,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                        child: AutoSizeText(
                                                          value,
                                                          style: Theme.of(context).textTheme.bodyText2,
                                                          maxFontSize: 15,
                                                        ),
                                                      ),
                                                    );
                                                  }).toList(),
                                                  onChanged: (newPreset) {
                                                    setState(() {
                                                      _currentPreset = newPreset;
                                                      _currentCamera = cameras[0];
                                                      _currentRes = resolutions[0];
                                                      _currentFPS = FPSs[0];
                                                      if (_currentPreset == "Custom") {
                                                        isCustoming = true;
                                                        updateSelected();
                                                      } else
                                                        isCustoming = false;
                                                    });
                                                  },
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                      HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.6),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.01),
                                      HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.6),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                      // Camera
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          AutoSizeText(
                                            "Camera",
                                            style: Theme.of(context).textTheme.headline5,
                                            maxLines: 1,
                                            maxFontSize: 15,
                                          ),
                                          Container(
                                            color: Colors.white,
                                            width: MediaQuery.of(context).size.width * 0.32,
                                            height: MediaQuery.of(context).size.height * 0.07,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                isCustoming
                                                    ? InkWell(
                                                        child: Icon(Icons.arrow_back_ios, size: MediaQuery.of(context).size.width * 0.04, color: Colors.grey.shade100),
                                                        onTap: () => null,
                                                      )
                                                    : InkWell(
                                                        child: Icon(Icons.arrow_back_ios, size: MediaQuery.of(context).size.width * 0.04),
                                                        onTap: () => decreaseCameraIndex(),
                                                      ),
                                                AutoSizeText(
                                                  "${cameras[cIndex]}",
                                                  style: Theme.of(context).textTheme.bodyText2,
                                                  maxFontSize: 15,
                                                ),
                                                isCustoming
                                                    ? InkWell(
                                                        child: Icon(Icons.arrow_forward_ios, size: MediaQuery.of(context).size.width * 0.04, color: Colors.grey.shade100),
                                                        onTap: () => null,
                                                      )
                                                    : InkWell(
                                                        child: Icon(Icons.arrow_forward_ios, size: MediaQuery.of(context).size.width * 0.04),
                                                        onTap: () => increaseCameraIndex(),
                                                      ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                      // Resolution
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          AutoSizeText(
                                            "Resolution",
                                            style: Theme.of(context).textTheme.headline5,
                                            maxLines: 1,
                                            maxFontSize: 15,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                            child: DropdownButtonHideUnderline(
                                              child: Container(
                                                color: Colors.white,
                                                width: MediaQuery.of(context).size.width * 0.32,
                                                height: MediaQuery.of(context).size.height * 0.07,
                                                child: DropdownButton<String>(
                                                  value: _currentRes,
                                                  icon: Icon(Icons.arrow_drop_down),
                                                  items: availableChoices[_currentCamera]['res'].map<DropdownMenuItem<String>>((String value) {
                                                    return DropdownMenuItem<String>(
                                                      value: value,
                                                      child: Padding(
                                                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                        child: AutoSizeText(
                                                          value,
                                                          style: Theme.of(context).textTheme.bodyText2,
                                                          maxFontSize: 15,
                                                        ),
                                                      ),
                                                    );
                                                  }).toList(),
                                                  onChanged: isCustoming
                                                      ? (newRes) {
                                                          setState(() {
                                                            _currentRes = newRes;
                                                            updateSelected();
                                                          });
                                                        }
                                                      : null,
                                                  disabledHint: isCustoming
                                                      ? null
                                                      : Padding(
                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                          child: AutoSizeText(
                                                            '${presetsCFG[_currentPreset][_currentCamera].resolution}',
                                                            style: Theme.of(context).textTheme.bodyText2,
                                                            maxFontSize: 15,
                                                          ),
                                                        ),
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                                      // Frame rate
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          AutoSizeText(
                                            "Frame rate",
                                            style: Theme.of(context).textTheme.headline5,
                                            maxLines: 1,
                                            maxFontSize: 15,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                                            child: DropdownButtonHideUnderline(
                                              child: Container(
                                                color: Colors.white,
                                                width: MediaQuery.of(context).size.width * 0.32,
                                                height: MediaQuery.of(context).size.height * 0.07,
                                                child: DropdownButton<String>(
                                                    value: _currentFPS,
                                                    icon: Icon(Icons.arrow_drop_down),
                                                    items: availableChoices[_currentCamera]['fps'].map<DropdownMenuItem<String>>((String value) {
                                                      return DropdownMenuItem<String>(
                                                        value: value,
                                                        child: Padding(
                                                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                          child: AutoSizeText(
                                                            value,
                                                            style: Theme.of(context).textTheme.bodyText2,
                                                            maxFontSize: 15,
                                                          ),
                                                        ),
                                                      );
                                                    }).toList(),
                                                    onChanged: isCustoming
                                                        ? (newFPS) {
                                                            setState(() {
                                                              _currentFPS = newFPS;
                                                              updateSelected();
                                                            });
                                                          }
                                                        : null,
                                                    disabledHint: isCustoming
                                                        ? null
                                                        : Padding(
                                                            padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                                            child: AutoSizeText(
                                                              '${presetsCFG[_currentPreset][_currentCamera].FPS}',
                                                              style: Theme.of(context).textTheme.bodyText2,
                                                              maxFontSize: 15,
                                                            ),
                                                          )),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(height: MediaQuery.of(context).size.height * 0.04),
                                      // Button
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Opacity(
                                            opacity: isCustoming ? 1.0 : 0.0,
                                            child: finishedCustom ? addPresetButton() : customPrevButton(),
                                          ),
                                          isCustoming ? customNextButton() : customApplyButton()
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  bottomNavigationBar: InkWell(
                    onTap: () => cleanUpAndBack(),
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.1,
                      color: Theme.of(context).primaryColor,
                      child: Center(
                        child: AutoSizeText(
                          "BACK",
                          style: Theme.of(context).textTheme.button,
                          maxFontSize: 20,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Center(child: LoadingAnimation(radius: 50, dotRadius: 10)),
            ],
          );
  }

  void sendSetting() {
    debugPrint("Sending");
  }

  void sendCurrentCamera() {
    debugPrint("Sending $_currentCamera");
  }

  void addNewPreset() {
    debugPrint("Add new preset");
  }

  decreaseCameraIndex() {
    setState(() {
      cIndex = cIndex - 1;
      if (cIndex == -1) cIndex = cameras.length - 1;
      _currentCamera = cameras[cIndex];
      _currentFPS = FPSs[0];
      _currentRes = resolutions[0];
      sendCurrentCamera();
    });
  }

  increaseCameraIndex() {
    setState(() {
      cIndex = cIndex + 1;
      if (cIndex == cameras.length) cIndex = 0;
      _currentCamera = cameras[cIndex];
      _currentFPS = FPSs[0];
      _currentRes = resolutions[0];
      sendCurrentCamera();
    });
  }

  customNextCam() {
    setState(() {
      if (cIndex != cameras.length - 1) cIndex = cIndex + 1;
      _currentCamera = cameras[cIndex];
      _currentFPS = availableChoices[_currentCamera]['fps'][0];
      _currentRes = availableChoices[_currentCamera]['res'][0];
      updateSelected();
    });
  }

  customPrevCam() {
    setState(() {
      if (cIndex != 0) cIndex = cIndex - 1;
      applied.remove(_currentCamera);
      _currentCamera = cameras[cIndex];
      _currentFPS = availableChoices[_currentCamera]['fps'][0];
      _currentRes = availableChoices[_currentCamera]['res'][0];
      updateSelected();
    });
  }

  void updateSelected() {
    if (applied.containsKey(_currentCamera)) if (applied[_currentCamera].containsKey('fps'))
      applied[_currentCamera]['fps'] = _currentFPS;
    else
      applied[_currentCamera].putIfAbsent('fps', () => _currentFPS);

    if (applied.containsKey(_currentCamera)) if (applied[_currentCamera].containsKey('res'))
      applied[_currentCamera]['res'] = _currentRes;
    else
      applied[_currentCamera].putIfAbsent('res', () => _currentRes);
    else
      applied.putIfAbsent(_currentCamera, () => {});
    if (applied[_currentCamera].containsKey('fps'))
      applied[_currentCamera]['fps'] = _currentFPS;
    else
      applied[_currentCamera].putIfAbsent('fps', () => _currentFPS);

    if (applied[_currentCamera].containsKey('res'))
      applied[_currentCamera]['res'] = _currentRes;
    else
      applied[_currentCamera].putIfAbsent('res', () => _currentRes);
    calculateRemainChoices();
  }

  void calculateRemainChoices() {
    used = 0;
    applied.forEach((cam, camInfo) {
      print("#### $cam ####");
      camInfo.forEach((cfg, value) {
        print("$cfg, $value");
        used += usage[value];
      });
    });
    print("Used memory: $used / $memory");
    print("-------------------------------------------------------");
  }

  customNextButton() {
    if (cameras[cIndex] != 'Right') {
      return InkWell(
        splashColor: Colors.blue,
        highlightColor: Colors.blue,
        onTap: () => customNextCam(),
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
          width: MediaQuery.of(context).size.width * 0.27,
          height: MediaQuery.of(context).size.height * 0.05,
          child: Center(
            child: AutoSizeText(
              "Next",
              style: Theme.of(context).textTheme.button,
              maxFontSize: 20,
              maxLines: 1,
            ),
          ),
        ),
      );
    } else {
      return InkWell(
        splashColor: Colors.blue,
        highlightColor: Colors.blue,
        onTap: () => sendSetting(),
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
          width: MediaQuery.of(context).size.width * 0.27,
          height: MediaQuery.of(context).size.height * 0.05,
          child: Center(
            child: AutoSizeText(
              "Apply",
              style: Theme.of(context).textTheme.button,
              maxFontSize: 20,
              maxLines: 1,
            ),
          ),
        ),
      );
    }
  }

  customApplyButton() {
    return InkWell(
      onTap: () => sendSetting(),
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
        width: MediaQuery.of(context).size.width * 0.27,
        height: MediaQuery.of(context).size.height * 0.05,
        child: Center(
          child: AutoSizeText(
            "Apply",
            style: Theme.of(context).textTheme.button,
            maxFontSize: 20,
            maxLines: 1,
          ),
        ),
      ),
    );
  }

  addPresetButton() {
    return InkWell(
      onTap: () => addNewPreset(),
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
        width: MediaQuery.of(context).size.width * 0.27,
        height: MediaQuery.of(context).size.height * 0.05,
        child: Center(
          child: AutoSizeText(
            "Add Preset",
            style: Theme.of(context).textTheme.button,
            maxFontSize: 20,
            maxLines: 1,
          ),
        ),
      ),
    );
  }

  customPrevButton() {
    return Opacity(
      opacity: cameras[cIndex] == 'Front-wide' ? 0.0 : 1.0,
      child: InkWell(
        splashColor: cameras[cIndex] != 'Front-wide' ? Colors.blue : Colors.transparent,
        highlightColor: cameras[cIndex] != 'Front-wide' ? Colors.blue : Colors.transparent,
        onTap: () => cameras[cIndex] != 'Front-wide' ? customPrevCam() : null,
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
          width: MediaQuery.of(context).size.width * 0.27,
          height: MediaQuery.of(context).size.height * 0.05,
          child: Center(
            child: AutoSizeText(
              "Previous",
              style: Theme.of(context).textTheme.button,
              maxFontSize: 20,
              maxLines: 1,
            ),
          ),
        ),
      ),
    );
  }
}
