import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:protomate/ui/common.dart';

class HUDSettingPage extends StatefulWidget {
  @override
  _HUDSettingPageState createState() => _HUDSettingPageState();
}

class _HUDSettingPageState extends State<HUDSettingPage> {
  static final List<String> modes = ['Mode 1', 'Mode 2', 'Mode 3', "Mode 4", 'Mode 5', "Mode 6"];
  String _currentMode = modes[0];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Theme.of(context).accentColor,
          ),
          Column(
            children: <Widget>[
//              HelmetStatus(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  AutoSizeText(
                    "Mode",
                    style: Theme.of(context).textTheme.headline2,
                    maxFontSize: 30,
                  ),
                  DropdownButtonHideUnderline(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      height: MediaQuery.of(context).size.height * 0.05,
                      color: Colors.white,
                      child: DropdownButton(
                          value: _currentMode,
                          icon: Icon(Icons.arrow_drop_down),
                          items: modes.map((String mode) {
                            return DropdownMenuItem<String>(
                              value: mode,
                              child: Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                child: AutoSizeText(
                                  mode,
                                  style: Theme.of(context).textTheme.bodyText2,
                                  maxFontSize: 16,
                                ),
                              ),
                            );
                          }).toList(),
                          onChanged: (newMode) {
                            setState(() {
                              _currentMode = newMode;
                            });
                          }),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                child: HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.9),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              Padding(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.02),
                child: HorizontalLine(lheight: MediaQuery.of(context).size.height * 0.001, lwidth: MediaQuery.of(context).size.width * 0.9),
              ),
              AutoSizeText(
                "HUD Display Preview",
                style: Theme.of(context).textTheme.headline2,
                maxFontSize: 30,
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.02),
              Container(
                width: MediaQuery.of(context).size.width * 0.9,
                height: (MediaQuery.of(context).size.width * 0.9) / 16 * 9,
                color: Colors.black,
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.03),
              Padding(
                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.3,
                      height: MediaQuery.of(context).size.height * 0.05,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Theme.of(context).buttonColor,
                      ),
                      child: Center(
                        child: AutoSizeText(
                          "Apply",
                          style: Theme.of(context).textTheme.button,
                          maxFontSize: 20,
                          maxLines: 1,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
      bottomNavigationBar: InkWell(
        onTap: () => Navigator.pop(context),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          color: Theme.of(context).primaryColor,
          child: Center(
            child: AutoSizeText(
              "BACK",
              style: Theme.of(context).textTheme.button,
              maxFontSize: 20,
            ),
          ),
        ),
      ),
    );
  }
}
