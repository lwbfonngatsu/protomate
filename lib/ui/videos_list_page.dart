import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class VideoListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: InkWell(
        onTap: () => Navigator.pop(context),
        child: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          color: Theme.of(context).primaryColor,
          child: Center(
            child: AutoSizeText(
              "BACK",
              style: Theme.of(context).textTheme.button,
              maxFontSize: 20,
            ),
          ),
        ),
      ),
    );
  }
}