import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:protomate/ui/common.dart';
import 'package:protomate/ui/images_list_page.dart';
import 'package:protomate/ui/videos_list_page.dart';

class DownloadFilePage extends StatelessWidget {
  const DownloadFilePage();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Theme.of(context).accentColor,
        ),
        Column(
          children: <Widget>[
            HelmetStatus(),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
              child: Center(
                child: Container(
                  child: AutoSizeText(
                    "ProtoMate",
                    style: Theme.of(context).textTheme.headline1,
                    maxFontSize: 56,
                    maxLines: 1,
                  ),
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.38,
              child: Image.asset("images/Home_Protomate.png"),
            ),
            InkWell(
              child: InkWell(
                onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (context) => ImagesListPage())),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.image, color: Colors.white, size: 34.0),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: AutoSizeText(
                          "Images",
                          style: Theme.of(context).textTheme.headline4,
                          maxFontSize: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
              child: InkWell(
                onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (context) => VideoListPage())),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.video_library, color: Colors.white, size: 34.0),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: AutoSizeText(
                          "Videos",
                          style: Theme.of(context).textTheme.headline4,
                          maxFontSize: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
