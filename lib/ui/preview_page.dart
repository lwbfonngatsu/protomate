import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:protomate/ui/common.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:provider/provider.dart';

class PreviewPage extends StatefulWidget {
  @override
  _PreviewPageState createState() => _PreviewPageState();
}

class _PreviewPageState extends State<PreviewPage> {
  bool show = true;
  int mode = 0;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (context.read<StreamListener>().remoteRenderer != null) await context.read<StreamListener>().remoteRenderer.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.black,
        ),
        GestureDetector(
          onTap: () => onClickScreen(),
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.8,
                color: Colors.black,
                child: RTCVideoView(
                  context.watch<StreamListener>().remoteRenderer,
                  mirror: false,
                  objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitContain,
                ),
              ),
              Expanded(
                child: Container(
                  color: Colors.black,
                ),
              )
            ],
          ),
        ),
        AnimatedOpacity(
          duration: Duration(milliseconds: 300),
          opacity: show ? 1.0 : 0.0,
          child: Column(
            children: <Widget>[
              HelmetStatus(),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.65,
              ),
              CarouselSlider(
                items: <Widget>[
                  Container(child: AutoSizeText("PHOTO", style: Theme.of(context).textTheme.headline6, maxFontSize: 14)),
                  Container(child: AutoSizeText("VIDEO", style: Theme.of(context).textTheme.headline6, maxFontSize: 14))
                ],
                options: CarouselOptions(viewportFraction: 1.0, height: MediaQuery.of(context).size.height * 0.05, enableInfiniteScroll: false, enlargeCenterPage: true, onPageChanged: onPageChange),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.17,
                height: MediaQuery.of(context).size.width * 0.17,
                child: (mode == 0) ? snapShotButton() : recordButton(),
              ),
            ],
          ),
        )
      ],
    );
  }

  snapShotButton() {
    return FloatingActionButton(
      onPressed: () {
        debugPrint("Snapshot");
      },
      backgroundColor: Colors.white,
      child: Stack(
        children: <Widget>[
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.15,
              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black),
            ),
          ),
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.12,
              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  recordButton() {
    return FloatingActionButton(
      onPressed: () {
        debugPrint("Record");
      },
      backgroundColor: Colors.white,
      child: Stack(
        children: <Widget>[
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.15,
              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black),
            ),
          ),
          Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.12,
              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  onClickScreen() {
    setState(() {
      show = !show;
    });
  }

  onPageChange(int index, CarouselPageChangedReason changeReason) {
    setState(() {
      mode = index;
    });
  }
}
