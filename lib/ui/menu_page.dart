import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:protomate/ui/common.dart';
import 'package:protomate/ui/download_file_page.dart';
import 'package:protomate/ui/setting_page.dart';

class Menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Theme.of(context).accentColor,
          ),
          Column(
            children: <Widget>[
              HelmetStatus(),
              Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
                child: Center(
                  child: Container(
                    child: AutoSizeText(
                      "ProtoMate",
                      style: Theme.of(context).textTheme.headline1,
                      maxFontSize: 56,
                      maxLines: 1,
                    ),
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.38,
                child: Image.asset("images/Home_Protomate.png"),
              ),
              InkWell(
                child: InkWell(
                  onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (context) => SettingPage())),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.05,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.settings, color: Colors.white, size: 34.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: AutoSizeText(
                            "Setting",
                            style: Theme.of(context).textTheme.headline4,
                            maxFontSize: 20,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Spacer(),
              InkWell(
                onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (context) => DownloadFilePage())),
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  height: MediaQuery.of(context).size.height * 0.05,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.file_download, color: Colors.white, size: 34.0),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: AutoSizeText(
                          "Download Files",
                          style: Theme.of(context).textTheme.headline4,
                          maxFontSize: 20,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.05),
                child: InkWell(
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.05,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Theme.of(context).buttonColor),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.map, color: Colors.white, size: 34.0),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: AutoSizeText(
                            "Map",
                            style: Theme.of(context).textTheme.headline4,
                            maxFontSize: 20,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              Spacer()
            ],
          ),
        ],
      ),
      bottomNavigationBar: InkWell(
        onTap: () {
          Provider.of<StatusListener>(context, listen: false).stopBlueService();
        },
        child: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          color: Theme.of(context).primaryColor,
          child: Center(
            child: AutoSizeText(
              "BACK",
              style: Theme.of(context).textTheme.button,
              maxFontSize: 20,
            ),
          ),
        ),
      ),
    );
  }
}
