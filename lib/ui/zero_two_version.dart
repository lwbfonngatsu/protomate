import 'package:flutter/material.dart';
import 'package:protomate/ui/bluetooth_page.dart';
import 'package:protomate/ui/common.dart';
import 'package:protomate/ui/download_file_page.dart';
import 'package:protomate/ui/preview_page.dart';
import 'package:protomate/ui/setting_page.dart';
import 'package:provider/provider.dart';

class MainNavigateBar extends StatefulWidget {
  @override
  _MainNavigateBarState createState() => _MainNavigateBarState();
}

class _MainNavigateBarState extends State<MainNavigateBar> {
  List<Widget> pages = <Widget>[
    BluetoothPage(),
    const SettingPage(),
    PreviewPage(),
    const DownloadFilePage(),
//    null
  ];

  void onMenuTapped(int index) {
//    if (!context.read<StatusListener>().connected){
//      return;
//    }
    setState(() {
      context.read<GlobalState>().currentPageIndex = index;
      if (context.read<GlobalState>().currentPageIndex == 2 && !context.read<StreamListener>().streamConnected){
        Provider.of<StreamListener>(context, listen: false).connectStream();
      }
      else if (context.read<GlobalState>().currentPageIndex == 2 && context.read<StreamListener>().streamConnected){
        Provider.of<StreamListener>(context, listen: false).startStream();
      }
      if (context.read<GlobalState>().currentPageIndex != 2 && context.read<StreamListener>().streamConnected){
        Provider.of<StreamListener>(context, listen: false).cleanupStream();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: context.watch<GlobalState>().currentPageIndex,
        children: pages,
      ),
      bottomNavigationBar: Theme(
        data: ThemeData(
          splashColor: context.watch<StatusListener>().connected ? Color(0x66C8C8C8) : Colors.transparent,
          highlightColor: context.watch<StatusListener>().connected ? Color(0x66C8C8C8) : Colors.transparent
        ),
        child: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.bluetooth), title: Text("Connect")),
            BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text("Settings")),
            BottomNavigationBarItem(icon: Icon(Icons.camera), title: Text("Preview")),
            BottomNavigationBarItem(icon: Icon(Icons.folder), title: Text("Files")),
//          BottomNavigationBarItem(icon: Icon(Icons.map), title: Text("Map")),
          ],
          currentIndex: context.watch<GlobalState>().currentPageIndex,
          selectedItemColor: Colors.black,
          onTap: (index) => onMenuTapped(index),
          unselectedItemColor: Colors.black12,
        ),
      ),
    );
  }
}
