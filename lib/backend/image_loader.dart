class Images {
  static List<Images> getImages() => [
        Images("images/penguin.png", "Penguin", "8/18/2020", "4:40 PM"),
        Images("images/front_wide.png", "Front-wide", "7/18/2020", "4:00 PM"),
        Images("images/front_tele.png", "Front-tele", "7/18/2020", "4:01 PM"),
        Images("images/left.png", "Left", "7/18/2020", "4:02 PM"),
        Images("images/rear.png", "Rear", "7/18/2020", "4:03 PM"),
        Images("images/right.png", "Right", "7/18/2020", "4:04 PM")
      ];

  String image;
  String title;
  String date;
  String time;

  Images(this.image, this.title, this.date, this.time);
}
