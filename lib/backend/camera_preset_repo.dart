class PresetRepo {
  static Map getPreset() => {
        'Preset 1': {
          'Front-wide': PresetRepo("4K", "60 FPS"),
          'Front-tele': PresetRepo("4K", "30 FPS"),
          'Left': PresetRepo("Full HD", "30 FPS"),
          'Rear': PresetRepo("Full HD", "30 FPS"),
          'Right': PresetRepo("Full HD", "30 FPS"),
        },
        'Preset 2': {
          'Front-wide': PresetRepo("4K", "30 FPS"),
          'Front-tele': PresetRepo("4K", "30 FPS"),
          'Left': PresetRepo("Full HD", "30 FPS"),
          'Rear': PresetRepo("4K", "30 FPS"),
          'Right': PresetRepo("Full HD", "30 FPS"),
        },
        'Preset 3': {
          'Front-wide': PresetRepo("Full HD", "60 FPS"),
          'Front-tele': PresetRepo("Full HD", "60 FPS"),
          'Left': PresetRepo("Full HD", "30 FPS"),
          'Rear': PresetRepo("Full HD", "30 FPS"),
          'Right': PresetRepo("Full HD", "30 FPS"),
        },
        'Preset 4': {
          'Front-wide': PresetRepo("Full HD", "30 FPS"),
          'Front-tele': PresetRepo("Full HD", "30 FPS"),
          'Left': PresetRepo("720P", "30 FPS"),
          'Rear': PresetRepo("Full HD", "30 FPS"),
          'Right': PresetRepo("720P", "30 FPS"),
        },
        'Preset 5': {
          'Front-wide': PresetRepo("720P", "30 FPS"),
          'Front-tele': PresetRepo("720P", "30 FPS"),
          'Left': PresetRepo("720P", "30 FPS"),
          'Rear': PresetRepo("720P", "30 FPS"),
          'Right': PresetRepo("720P", "30 FPS"),
        },
      };
  String resolution;
  String FPS;

  PresetRepo(this.resolution, this.FPS);
}
